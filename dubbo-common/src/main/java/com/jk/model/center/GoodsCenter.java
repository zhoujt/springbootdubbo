package com.jk.model.center;

import lombok.Data;

import java.io.Serializable;


@Data
public class GoodsCenter implements Serializable {


    private  Long  cenid;
    private  Long cnumber;
    private  Long colorid;
    private  Long unitid;
    private  String cname;
    private  String uname;
    private  String goodssize;


}
