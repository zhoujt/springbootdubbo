package com.jk.model.order;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;


import java.io.Serializable;
import java.util.Date;


@Data
public class Order  implements Serializable{

    private  Long  rid;
    private  Long  uid;
    private  Long  goodsid;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date orderdate;
    private String  goodssize;
    private String  unitname;
    private String  colorname;

    private String userPlayName;
    private Long loveClickCount;

    private String  goodsname;


}
