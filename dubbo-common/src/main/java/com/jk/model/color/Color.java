package com.jk.model.color;

import lombok.Data;

import java.io.Serializable;

@Data
public class Color implements Serializable {
    private  Long  cId;
    private  String cType;
    private  String  cName;
    private  Integer  cSort;
    private  String  cMessage;
    private  Long  cPid;
}
