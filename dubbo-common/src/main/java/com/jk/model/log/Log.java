/** 
 * <pre>项目名称:maven_easyui_user 
 * 文件名称:Log.java 
 * 包名:com.jk.zjt.model.log 
 * 创建日期:2018年11月22日下午3:06:40 
 * Copyright (c) 2018, yuxy123@gmail.com All Rights Reserved.</pre> 
 */  
package com.jk.model.log;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@Document(collection="sys_log")
public class Log implements Serializable{
private static final long serialVersionUID = 6994183174891006017L;
private String id;
private String userName;
private String className;
private String method;
private String reqParam;
private String respParam;
@DateTimeFormat(pattern="yyyy-MM-dd")
private Date createTime;
private String ip;
private String url;

private String startdate;
private String enddate;
}
