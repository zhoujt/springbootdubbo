package com.jk.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class TreeBean implements Serializable {

		private Integer id;
		
		private String text;
		
		private String href;
		
		private Integer pid;
		
		private Boolean checked;
		
		private List<TreeBean> nodes;

		
	}	

