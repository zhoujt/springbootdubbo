package com.jk.model.tree;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Tree implements Serializable {
    private Integer id;
    private String text;
    private Integer pid;
    private List<Tree> nodes; //子节点
    private String href;
    private Integer status;
 }


