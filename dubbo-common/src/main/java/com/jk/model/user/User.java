/** 
 * <pre>项目名称:maven_easyui_user 
 * 文件名称:User.java 
 * 包名:com.jk.zjt.model.user 
 * 创建日期:2018年11月12日下午6:48:25 
 * Copyright (c) 2018, yuxy123@gmail.com All Rights Reserved.</pre> 
 */  
package com.jk.model.user;

import java.io.Serializable;
import java.util.Date;



import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;


@Data
public class User implements Serializable {

private Long id;
private String loginName;
private String name;
private String password;
private Integer sex;
private Integer age;
private String phone;
private Integer userType;
private Integer status;
private Integer delFlag;
private String userImg;
@DateTimeFormat(pattern="yyyy-MM-dd")
private Date updateTime;
@DateTimeFormat(pattern="yyyy-MM-dd")
private Date createTime;
@DateTimeFormat(pattern="yyyy-MM-dd")
private Date birthday;
private Long deptId;
private String deptName;
private String jianjie;
private String roleNames;
private String roleId;
private String resourceNames;
private String validateCode;
private String usertypename;
private String startdate;
private String enddate;
private String email;


}
