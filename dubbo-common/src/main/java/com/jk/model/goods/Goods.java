package com.jk.model.goods;

import lombok.Data;

import java.io.Serializable;

@Data
public class Goods implements Serializable {
    /**
     * 商品主键
     */
    private Long gid;
    /**
     * 商品名称
     */
    private String gname;
    /**
     * 商品规格
     */
    private String gsize;
    /**
     * 商品课程  0:否 1：是
     */
    private Integer gclass;
    /**
     * 商品售价
     */
    private Double gprice;
    /**
     * 商品标签
     */
    private String glog;
    /**
     * 商品状态 0：上架 1：下架
     */
    private Integer gstatus;
    /**
     * 外键
     * 品名主键
     */
    private Long pid;
    /**
     * 外键
     * 品牌主键
     */
    private Long bid;
    /**
     * 外键
     * 分类主键
     */
    private Long tid;
    /**
     * 外键
     * 单位主键
     */
    private Long uid;
    /**
     * 外键
     * 颜色主键
     */
    private Long cid;
    /**
     * 外键
     * 品名主键
     */
    private String pname;
    /**
     * 外键
     * 颜色主键
     */
    private String bname;
    /**
     * 外键
     * 分类名称
     */
    private String text;
    /**
     * 外键
     * 单位名称
     */
    private String uname;
    /**
     * 外键
     * 颜色名称
     */
    private String cname;
    /*
    * 上传图片
    * */
    private String img;






}


