package com.jk.model.type;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Type implements Serializable {
    private Long id;
    private String text;
    private Long pid;
    private List<Type> nodes;
    private Boolean open =true;
    private String icon;//图标样式
    private String font;//字体
    private String name;
}
