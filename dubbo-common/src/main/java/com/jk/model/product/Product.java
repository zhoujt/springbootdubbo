package com.jk.model.product;

import lombok.Data;

import java.io.Serializable;

@Data
public class Product implements Serializable {

    /**
     * 主键
     */
    private long pId;

    /**
     *名称
     */
    private String pName;

    /**
     * 状态  0  上班   1   下架
     */
    private Integer pStatus =1;

    /**
     * 品牌id
     */
    private long bId;

    /**
     * 分类
     */
    private long tId;

    private String text;

    private String bname;




}
