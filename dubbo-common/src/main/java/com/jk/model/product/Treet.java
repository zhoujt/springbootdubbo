package com.jk.model.product;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Treet implements Serializable {
    private Integer id;
    private String text;
    private Integer pid;
    private List<Treet> nodes; //子节点
    private String href;

    private Boolean selectable;  //选择选项卡

 }
