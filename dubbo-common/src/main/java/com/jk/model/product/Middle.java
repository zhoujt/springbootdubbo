package com.jk.model.product;

import lombok.Data;

import java.io.Serializable;

@Data
public class Middle implements Serializable {


    /**
     * 主键id
     */
    private  Long  cenid;

    /**
     * 编号
     */
    private  Long cnumber;

    /**
     * 颜色id
     */
    private  Long colorid;

    /**
     * 单位id
     */
    private  Long unitid;

    /**
     * 规格id
     */
    private  String goodssize;

    private  String cname;
    private  String uname;
}
