package com.jk.model.unit;

import lombok.Data;

import java.io.Serializable;

@Data
public class Unit implements Serializable {
    private Long unitId;
    private String unitType;
    private String unitName;
    private Integer unitSort;
    private String unitMessage;
    private Long unitPid;

}
