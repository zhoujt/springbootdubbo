package com.jk.model.brand;

import lombok.Data;

import java.io.Serializable;

@Data
public class Brand implements Serializable {

    private  Long bid;

    private String bfirstLetter;

    private String bname;

}
