package com.jk;

public class Config {
    /**
     * url前半部分
     */
    public static final String BASE_URL =  "https://api.miaodiyun.com/20150822/industrySMS/sendSMS";

    /**
     * 开发者注册后系统自动生成的账号，可在官网登录后查看
     */
    public static final String ACCOUNT_SID =  "ae16289919874b4d9bbbddd4966ea2f7";

    /**
     * 开发者注册后系统自动生成的TOKEN，可在官网登录后查看
     */
    public static final String AUTH_TOKEN =  "9e6021314d924ae9a118f4ce1835287e";
    /**
     * 发送短信模版ID
     */
    public static final String TEMPLATE_ID = "952478561";


    /**
     * 响应数据类型, JSON或XML
     */
    public static final String RESP_DATA_TYPE = "json";
}
