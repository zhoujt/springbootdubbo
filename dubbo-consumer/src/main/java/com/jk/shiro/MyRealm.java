package com.jk.shiro;



import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.user.User;
import com.jk.service.user.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


import java.util.List;


@Component
public class MyRealm extends AuthorizingRealm {

    @Reference
    private UserService userService;

    @Bean(name = "userService")
    public UserService getUserService(){
        return userService;
    }
    /**
     * shiro 的认证方法
     * */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //token 令牌 验证用户身份  token.getPrincipal返回浏览器中用户名输入框输入的内容
        String principal = (String)token.getPrincipal();
        User user = SpringBeanFactoryUtils.getBean("userService", UserService.class).findUserByUsername(principal);
        if(user == null){
            //如果用户名不为张三 则 抛出用户名不存在异常
            throw new UnknownAccountException();
        }
        System.out.println("token = [" + principal + "]");
        SecurityUtils.getSubject().getPrincipal();
        //认证方法
        //第一个参数就相当于session.setAttrbit
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(user,user.getPassword(),this.getName());
        return simpleAuthenticationInfo;
    }




    /**
     * shiro 的授权方法
     * */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        // 1. 从shiro session中获取用户信息
        User user = (User)principalCollection.getPrimaryPrincipal();
        List<String> keyList = SpringBeanFactoryUtils.getBean("userService", UserService.class).queryPowerKeyList(user.getId());
        //创建授权
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.addStringPermissions(keyList);
        return simpleAuthorizationInfo;
//        User user = (User)principalCollection.getPrimaryPrincipal();
//        List<String> powerKeyList = new ArrayList();
//        powerKeyList = userService.queryPowerKeyList(user.getUserId());
//
//        //创建授权器
//        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
//        simpleAuthorizationInfo.addStringPermissions(powerKeyList);

    }

}
