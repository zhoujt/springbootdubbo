package com.jk.controller.menu;

import com.jk.model.user.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import sun.security.krb5.internal.ccache.CredentialsCache;

@RequestMapping("menu")
@Controller
public class MenuController {
	@RequestMapping("toalipay")
	public String toalipay(){
		return "alipay";
	}
	@GetMapping("tofile")
	@RequiresPermissions("menu:tofile")
	public String tofile() {

		return "/file";
	}
	@RequestMapping("tologin")
	public String tologin(){

		return "/view/login";
	}
@RequestMapping("todata")
public String todata(Model model) {
	User user=(User) SecurityUtils.getSubject().getPrincipal();
	model.addAttribute("user",user);
	return "/view/data";
	}
@RequestMapping("toresourcelist")
public String toresourcelist()
{
	return "/view/resourcelist";
}
@RequestMapping("toUnit")
public String toUnit() {

		return "/view/unit/UnitList";
}
@RequestMapping("tobresourcelist")
	public String tobresourcelist() {

		return "/view/bresourcelist";
	}
	@RequestMapping("toclasspie")
	public String toclasspie() {

		return "view/echarts/classpie";
	}
	@RequestMapping("toAddUnit")
	public String toAddUnit(){

		return "/view/unit/AddUnit";
	}
	@RequestMapping("toType")
	public String toType(){
		return "/view/type/TypeTree";
	}
	@RequestMapping("tologin2")
	public String tologin2(){
		return "/view/login2";
	}

}


