/** 
 * <pre>项目名称:maven_easyui_user 
 * 文件名称:LogController.java 
 * 包名:com.jk.zjt.controller.log 
 * 创建日期:2018年11月22日下午6:35:19 
 * Copyright (c) 2018, yuxy123@gmail.com All Rights Reserved.</pre> 
 */  
package com.jk.controller.log;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jk.model.log.Log;
import io.lettuce.core.GeoArgs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.data.mongodb.core.query.Query;



@Controller
@RequestMapping("log")
public class LogController {
	@Autowired
	private MongoTemplate mongoTemplate;

		@RequestMapping("tolog")
		public  String tolog(){
			return "/view/log/tolog";
		}


	//查询日志
	@RequestMapping("queryLogPage")
	@ResponseBody
	public List<Log> queryLogPage(Integer page, Integer rows, Log log){
		// find: 带有条件查询的查询方法，查询条件为Query对象
		// Query：设置条件查询参数
		Query query = new Query();
		Map<String, Object> pageResult=new HashMap<>();
		List<Log> find=mongoTemplate.find(query,Log.class );

		return find;
	}



	@ResponseBody
	@RequestMapping("findClassPie")
	public List<Map> selectClassPie() {
		Aggregation aggregation1 = Aggregation.newAggregation(Aggregation.group("className").first("className").as("name").count().as("value"));
		List<Map> mappedResults =mongoTemplate.aggregate(aggregation1, "sys_log", Map.class).getMappedResults();
		return mappedResults;
	}
}
