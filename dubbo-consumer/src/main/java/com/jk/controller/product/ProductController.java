package com.jk.controller.product;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.brand.Brand;
import com.jk.model.color.Color;
import com.jk.model.goods.Goods;
import com.jk.model.product.Middle;
import com.jk.model.product.Product;
import com.jk.model.product.Treet;
import com.jk.model.type.Type;
import com.jk.model.unit.Unit;
import com.jk.model.user.User;
import com.jk.service.Product.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("product")
public class ProductController {

    @Reference
    private ProductService productService;

    @RequestMapping("toProduct")
    public String tolist() {
        return "/view/product/productlistcurd";
    }


    //查询
    @RequestMapping("findAllProduct")
    @ResponseBody
    public  List<Product> findAllProduct(Model model, Product product){
        List<Product> productList = productService.findAllProduct(product);
        return productList;
    }

    @RequestMapping("toAdd")
    public String toAdd() {
        return "/view/product/productsave";
    }


    //新增
    @RequestMapping("addProduct")
    public String addProduct(Product product){
        productService.addProduct(product);
        return "redirect:toProduct";
    }

    //删除
    @RequestMapping("deleteProduct")
    @ResponseBody
    public void deleteProduct(long pid) {

        productService.deleteProduct(pid);
    }
    @RequestMapping("togoods")
    public String togoods(){
        return "view/goods/queryGoods";
    }

    //跳转添加商品页面
    @RequestMapping("toupdate")
    public String toupdate(Model model,long pid,String pname){
        model.addAttribute("pId",pid);
        model.addAttribute("pName",pname);
        return "/view/product/productGoods";
    }
    //回显
    @RequestMapping("goosderById")
    @ResponseBody
    public List<Product> goosderById(Long pid) {
        List<Product> goolist = productService.goosderById(pid);
        return goolist;
    }

    //添加商品
    @RequestMapping("addinsertProduct")
    public String addinsertProduct(Goods goods){
        goods.setGstatus(0);
        goods.setGclass(1);

        productService.addinsertProduct(goods);
        return "redirect:togoods";
    }
    //跳修改页面
    @RequestMapping("toupdateedit")
    public String toupdateedit(){
        return "/view/product/productupEnty";
    }
    //回显
    @RequestMapping("queryproductOrderById")
    @ResponseBody
    public Product queryproductOrderById(Long pid) {
         Product product = productService.queryproductOrderById(pid);
        return product;
    }
     //修改
    @RequestMapping("edit")
    public String edit(Product product){
        productService.edit(product);
        return "redirect:toProduct";
    }

    //品牌
    @RequestMapping("brandlist")
    @ResponseBody
    public List<Brand>brandlist(){
        List<Brand> brandlist = productService.brandlist();
        return brandlist;
    }

    //分类
    @RequestMapping("typelist")
    @ResponseBody
    public List<Type>typelist(){
        List<Type> typetlist = productService.typelist();
        return typetlist;
    }

    //单位
    @RequestMapping("unittypelist")
    @ResponseBody
    public List<Unit>unittypelist(){
        List<Unit> typetlist = productService.unittypelist();
        return typetlist;
    }

    //颜色
    @RequestMapping("colortypelist")
    @ResponseBody
    public List<Color>colortypelist(){
        List<Color> coletlist = productService.colortypelist();
        return coletlist;
    }

    //导航查询
    @RequestMapping("totreelist")
    @ResponseBody
    public List<Treet> totreelist(Integer id){
        return productService.totreelist(id);
    }

    @RequestMapping("toindxtree")
    public String todata() {
        return "/view/product/indextree";
    }


    /*//
    @RequestMapping("tomiddle")
    public String tomiddle(Middle middle) {
         productService.tomiddle(middle);
        return "/view/product/productmiddlesave";
    }*/

    //查询商品表
    @RequestMapping("centerlist")
    public List<Goods>centerlist(){
        return productService.centerlist();

    }
    @RequestMapping("toindxcenter")
    public String toindxcenter() {
        return "/view/product/productmiddlesave";
    }
    //新增
    @RequestMapping("tomiddle")
    @ResponseBody
    public String tomiddle(Goods goods,Middle middle) {
        goods.setGstatus(1);
        goods.setGclass(1);
        productService.tomiddle(goods);
        productService.tomiddlecenter(middle);
        return "redirect:togoods";
    }

    //注册跳转
    @RequestMapping("toregter")
    public String toregter() {
        return "/view/zhuce";
    }

    /**
     * 注册
     * @return
     */
    @RequestMapping("numbertest")
    @ResponseBody
    public HashMap<String, Object>numbertest(String loginName){
        HashMap<String,Object> hashMap = new HashMap<>();
        User user = productService.getUserListt2(loginName);
        if(user != null) {
            hashMap.put("code", 1);
            hashMap.put("msg", "用户名已存在，请从新输入");
            return hashMap;
        }
        hashMap.put("code", 0);
        hashMap.put("msg", "用户名可以用");
        return hashMap;
    }

    /**
     * 注册
     */
    @RequestMapping("insertUser")
    @ResponseBody
    private HashMap<String, Object>insertUser(User user) {
        HashMap<String,Object> hashMap = new HashMap<>();
        String loginName = user.getLoginName();
        User user1 = productService.getUserListt2(loginName);
        if(user1 != null) {
            hashMap.put("code", 1);
            hashMap.put("msg", "用户名已存在，请从新输入");
            return hashMap;
        }
        productService.toUser(user);
        hashMap.put("code", 0);
        hashMap.put("msg", "注册成功");
        return hashMap;
    }


}
