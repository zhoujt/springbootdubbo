package com.jk.controller.poi;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.goods.Goods;
import com.jk.service.poi.PoiService;
import com.jk.utils.ExcelImport;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("poi")
public class PoiController {
    @Reference
    private PoiService  poiService;
    @RequestMapping("addPoi")
    @ResponseBody
    public String uploadExcel(MultipartFile file) throws IOException {
        List<String[]> readExcel = ExcelImport.readExcel(file);
        List<Goods> list = new ArrayList<Goods>();
        for (int i = 0; i < readExcel.size(); i++) {
            String[] strings = readExcel.get(i);
            Goods goods = new Goods();
            for (int j = 0; j < strings.length; j++) {
                goods.setGname(strings[0]);
                goods.setGclass(Integer.valueOf(strings[1]));
                goods.setGprice(Double.valueOf(strings[2]));
                goods.setUid(Long.parseLong(strings[3]));
                goods.setCid(Long.parseLong(strings[4]));
                goods.setPid(Long.parseLong(strings[5]));
                goods.setBid(Long.parseLong(strings[6]));
                goods.setTid(Long.parseLong(strings[7]));
                goods.setGstatus(Integer.valueOf(strings[8]));

            }
            list.add(goods);
        }
        poiService.addGoods(list);
    return "1";
    }
}
