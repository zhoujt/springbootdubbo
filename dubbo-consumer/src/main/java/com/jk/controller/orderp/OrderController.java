package com.jk.controller.orderp;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.order.Order;
import com.jk.model.user.User;
import com.jk.service.orderp.OrderService;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("order")
public class OrderController {

    @Reference
    private OrderService orderService;


    @RequestMapping("findOrderList")
    @ResponseBody
    public List<Order> queryOrderList(){
        User user=(User) SecurityUtils.getSubject().getPrincipal();
        long userId = user.getId();
        return orderService.queryOrderList(userId);
    }


    @RequestMapping("toOrder")
    public String toOrder(){
        return "view/orderpage/orderpage";
    }

    @RequestMapping("deleteOrder")
    @ResponseBody
    public void  deleteOrder(Long rid){
        orderService.deleteOrder(rid);
    }
 }
