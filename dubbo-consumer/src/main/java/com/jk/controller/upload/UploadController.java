package com.jk.controller.upload;


import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.service.goods.GoodsService;
import com.jk.utils.AliyunOSSUtil;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by lightClouds917
 * Date 2018/2/7
 * Description:文件上传
 */
@Controller
@RequestMapping("upload")
public class UploadController {
    private final org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());

    @Reference
    private GoodsService goodsService;
    /**
     * 文件上传
     * 返回路径
     * @param file
     */

    @RequestMapping(value = "uploadBlog",method = RequestMethod.POST)
    public String uploadBlog(MultipartFile file,Long gid){


        logger.info("============>文件上传");
        try {
            if(null != file){
                String filename = file.getOriginalFilename();
                if(!"".equals(filename.trim())){
                    File newFile = new File(filename);
                    /*写入本地*/
                  /*  FileOutputStream os = new FileOutputStream(newFile);
                    os.write(file.getBytes());
                    os.close();
                    file.transferTo(newFile);*/
                    //上传到OSS
                    String uploadUrl = AliyunOSSUtil.upload(newFile);
                    System.out.println(uploadUrl);
                    goodsService.uploadBlog(uploadUrl,gid);

                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return "view/goods/queryGoods";

    }

    @RequestMapping(value = "toUploadBlog",method = RequestMethod.GET)
    public String toUploadBlog(){
        return "file";
    }
//文件下载 ?拼接一个参数filename=？?
    @RequestMapping("download")
    public void download(HttpServletRequest request, HttpServletResponse response){
        AliyunOSSUtil.download(request,response);
    }
}
