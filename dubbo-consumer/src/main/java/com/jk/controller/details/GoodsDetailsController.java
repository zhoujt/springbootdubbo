package com.jk.controller.details;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.color.Color;
import com.jk.model.order.Order;
import com.jk.model.center.GoodsCenter;
import com.jk.model.goods.Goods;
import com.jk.model.unit.Unit;
import com.jk.model.user.User;
import com.jk.service.details.DetailsService;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("goodsDetails")
public class GoodsDetailsController {

    @Reference
    private DetailsService detailsService;

  /*  @RequestMapping("seleDetailsById")
    public String seleGoodsDetails(Long gid, Model model) {
        List<Goods> url = detailsService.seleGoodsDetails(gid);
        model.addAttribute("url", url);
        return "view/color/goods";
    }*/

    @RequestMapping("seleColor")
    public String seleColor(Model model,Long cnumber,Long gid) {
        List<Goods> url = detailsService.seleGoodsDetails(gid);
        List<GoodsCenter> list = detailsService.seleColor(gid);
        User user=(User)SecurityUtils.getSubject().getPrincipal();
        model.addAttribute("user", user);
        model.addAttribute("url", url);
        model.addAttribute("list",list);
        return "view/color/goods";
    }
    @RequestMapping("addList")
    @ResponseBody
    public String addList(Goods goods,String colorname,String unitname, GoodsCenter goodsCenter){
      Order order= new Order();
      User user=(User)SecurityUtils.getSubject().getPrincipal();
      order.setUid( user.getId());
      order.setGoodsid(goods.getGid());
        order.setOrderdate(new Date());
        order.setColorname(colorname);
        order.setGoodssize(goodsCenter.getGoodssize());
        order.setUnitname(unitname);
        detailsService.addList(order);
        return  "1";
}
    public String  queryOrder(Model model){
        User user=(User)SecurityUtils.getSubject().getPrincipal();
       Long userId=user.getId();
        List<Order> ord=detailsService.queryOrder(userId);
        model.addAttribute("ord",ord);
        return "view/color/orderlist";

    }
    @RequestMapping("deleteOrder")
    public String   deleteOrder(Long rid){
    detailsService.deleteOrder(rid);
    return "redirect:queryOrder";
}
}