package com.jk.controller.user;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jk.Config;
import com.jk.ConstantsConf;
import com.jk.model.user.User;
import com.jk.service.user.UserService;

import com.jk.utils.HttpClientUtil;
import com.jk.utils.ValidateCodeUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.authc.UnknownAccountException;

import org.apache.shiro.authc.AuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;



import javax.annotation.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("user")
public class UserController {
    @Reference
    UserService userService;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;


    @RequestMapping("login")
    public String loginFail(HttpServletRequest request, ModelMap map,User user){
        String exceptionClassName =(String) request.getAttribute("shiroLoginFailure");
        if (null == "user.validateCode" || !"user.validateCode".equalsIgnoreCase("session")) {
            map.put("message","验证码错误");
            return "/view/login";
        }

        if(UnknownAccountException.class.getName().equals(exceptionClassName) || AuthenticationException.class.getName().equals(exceptionClassName)){
            System.out.println("用户名错误");
            map.put("message","用户名错误");
        }else{
            System.out.println("密码错误");
            map.put("message","密码错误");
        }
        return "/view/login";
    }




    @RequestMapping("findAllUser")
    @ResponseBody
    public  HashMap<String,Object> selectAllUser(User user){
        HashMap<String, Object> usermap = new HashMap<>();
        List<User> userlist =  userService.selectAllUser();
        usermap.put("rows",userlist);
        return usermap;
    }
    @RequestMapping("findAllUserb")
    public String selectAllUserb(Model model, User user){
        List<User> userlist = userService.selectAllUser();
        model.addAttribute("userlist",userlist);
        return "/view/bresourcelist";
    }
    @RequestMapping("addUser")
    public String addUser(User user){
        if(user.getId()==null||"".equals(user.getId())){
            userService.saveUser(user);
        }else{
            userService.updateUser(user);
        }
        return "redirect:findAllUserb";
    }
    @RequestMapping("findUserById")
    @ResponseBody
    public User findUserById(User user){

        return userService.findUserById(Integer.valueOf(user.getId().toString()));
    }

    @RequestMapping("deleteUserById")
    public String deleteUserById(User user){
        userService.deleteUserById(Integer.valueOf(user.getId().toString()));
        return "redirect:findAllUserb";
    }
    @RequestMapping("toIndex")
    public String toIndex(){
        return "index";
    }


    /**
     * 获取验证码图片以及验证码code值
     */
    @RequestMapping("validateCode")
    public void validateCode(
            HttpServletRequest request,
            HttpServletResponse response){

        // 设置响应的类型格式为图片格式
        response.setContentType("image/jpeg");
        //禁止图像缓存。
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);


        //获取session对象
        HttpSession session = request.getSession();

        //获取验证码图片以及验证码code值
        ValidateCodeUtil vCode = new ValidateCodeUtil(100,35,4,100);

        //将验证码code值存入session当中
        session.setAttribute("code", vCode.getCode());


        try {
            //将验证码图片写入到jsp页面
            vCode.write(response.getOutputStream());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    //验证码
    @RequestMapping("sendSmsContent")
    @ResponseBody
    public String sendSmsContent(String phone){
        HashMap<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("accountSid", Config.ACCOUNT_SID);
        paramMap.put("templateid", Config.TEMPLATE_ID);

        //随机6位数字 验证码
        String validateCode = String.valueOf(new Random().nextInt(899999) + 100000);
        paramMap.put("param", validateCode+","+5);

        //将验证码存入redis中
        String key = ConstantsConf.SMS_VALIDATE_CODE + "_" + phone;
        redisTemplate.opsForValue().set(key, validateCode, 5, TimeUnit.MINUTES);

        paramMap.put("to", phone);
        //24小时制
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String nowTime = sdf.format(new Date());
        paramMap.put("timestamp", nowTime);
        paramMap.put("sig", DigestUtils.md5Hex(Config.ACCOUNT_SID + Config.AUTH_TOKEN + nowTime));

        String resJson = HttpClientUtil.post(Config.BASE_URL, paramMap);
        JSONObject parseObject = JSON.parseObject(resJson);
        String respCode = parseObject.get("respCode").toString();
        if("00000".equals(respCode)){
            // 请求成功
            return "短信验证码发送成功";
        } else {
            // 请求失败
            return "短信验证码发送失败";
        }

    }
    @RequestMapping("findkeylist")
    @ResponseBody
    public List<String> findkeylist(){
      return  userService.queryPowerKeyList(43l);
    }
    @RequestMapping("userLoginByPhone")
    @ResponseBody
    public String userLoginByPhone(String phone, String smsCode, HttpSession session){
        // 1: 登录成功   2: 手机号为未注册  3: 验证码错误
        String msgFlag = "-1";

        // 1. 通过手机号查询用户是否已注册
        User user = userService.selectUserByPhone(phone);

        if(user == null){
            // 1.1 手机号关联不到用户，则提示手机号未注册用户
            msgFlag = "2";
        } else {
            // 1.2 手机号已经注册，判断验证码
            // 2. 判断输入的验证码和redis中验证码是否一致
            String redisCode = redisTemplate.opsForValue().get(ConstantsConf.SMS_VALIDATE_CODE + "_" + phone).toString();
            if(smsCode.equals(redisCode)){
                // 2.2 验证码一致，提示登录成功
                session.setAttribute("suser", user);
                msgFlag = "1";
            } else {
                // 2.1 验证码不一致，提示验证码错误
                msgFlag = "3";
            }
        }

        return msgFlag;

    }


}

