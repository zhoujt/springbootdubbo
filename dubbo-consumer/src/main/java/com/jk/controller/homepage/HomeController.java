package com.jk.controller.homepage;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.type.Type;
import com.jk.service.homepage.HomeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("home")
public class HomeController {
    @Reference
    private HomeService homeService;
    @RequestMapping("toHome")
    public String selectTypeByPid(Model model){
        List<Type> list = homeService.selectTypeByPid();
        model.addAttribute("list",list);
        return "/view/hemo";
    }




    @RequestMapping("queryTypeById")
    @ResponseBody
    public List<Type> selectTypeById(Model model){
        List<Type> lists = homeService.selectTypeById();
        model.addAttribute("lists",lists);
        return lists;
    }
}
