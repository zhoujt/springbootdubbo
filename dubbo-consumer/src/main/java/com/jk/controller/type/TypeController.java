package com.jk.controller.type;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.type.Type;
import com.jk.service.type.TypeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("type")
public class TypeController {
    @Reference
    private TypeService typeService;
    @RequestMapping("queryTypeTree")
    @ResponseBody
    public List<Type> selectTypeTree(){
        return typeService.selectTypeTree();
    }
    @RequestMapping("saveTypeByPid")
    @ResponseBody
    public void addTypeByPid(Type type){
        typeService.addTypeByPid(type);
    }
    @RequestMapping("addById")
    @ResponseBody
    public void addById(Type type){
        typeService.addById(type);
    }

}
