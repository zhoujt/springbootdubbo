package com.jk.controller.color;
import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.color.Color;
import com.jk.model.goods.Goods;
import com.jk.service.color.GoodsColorService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("color")
public class GoodsColorController {
    @Reference
    private GoodsColorService goodsColorServiceice;



    @RequestMapping("sele")
    public String seleGoodsColor() {
        return "view/color/ha";
    }

    @RequestMapping("seleList")
    @ResponseBody
    public List<Color> seleGoodsColorList() {
        List<Color> list = goodsColorServiceice.seleGoodsColorList();
        return list;
    }

    @RequestMapping("delete")
    @ResponseBody
    public String deleteById(Integer id) {
        goodsColorServiceice.deleteById(id);
        return "1";
    }
    @RequestMapping("addColor")
    public String addColor() {



        return "view/color/details";

    }
    @RequestMapping("seleListPid")
    @ResponseBody
    public List<Color> seleListPid() {
        List<Color> list = goodsColorServiceice.seleListPid();
        return list;
    }
    @RequestMapping("addList")
    public String addList(Color color) {
        color.setCType("商品管理-商品颜色");
        goodsColorServiceice.addList(color);
        return "redirect:sele";
    }

}