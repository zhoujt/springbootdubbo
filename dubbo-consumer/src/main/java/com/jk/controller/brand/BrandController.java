package com.jk.controller.brand;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.brand.Brand;
import com.jk.service.brand.BrandService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("brand")
public class BrandController {
    @Reference
    private BrandService brandService;

    @RequestMapping("tobrand")
    public String tobrand(){
        return "view/brand/brandlist";
    }

    /*@RequestMapping("findBrandList")
    public String selectBrandList(Model model,Brand brand){
        List<Brand> brandList =brandService.selectBrandList(brand);
        model.addAttribute("brandList",brandList);
        return "view/brand/brandlist";
    }*/

    @RequestMapping("findBrandList")
    @ResponseBody
    public List<Brand> selectBrandList(Brand brand){

     List<Brand> brandList =brandService.selectBrandList(brand);

        return brandList;

    }

    @PostMapping("addBrand")
    public String  saveBrand(Brand brand){
        if(brand.getBid()==null||"".equals(brand.getBid()))
        {
        brandService.saveBrand(brand);}
        else {
            brandService.updateBrand(brand);
        }
        return "redirect:tobrand";
    }


    /*@RequestMapping("findBrandById")
    public String selectBrandById(Long bid){
        Brand brand = brandService.selectBrandById(bid);

        return "redirect:tobrand" ;
    }*/

    @RequestMapping("findBrandById")
    @ResponseBody
    public Brand findUserById(Long bid){
        return brandService.selectBrandById(bid);
    }


    @RequestMapping("deleteBrand")
    @ResponseBody
    public void removeBrand(Long bid){
        brandService.removeBrand(bid);
    }





}
