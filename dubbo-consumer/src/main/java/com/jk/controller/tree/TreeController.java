package com.jk.controller.tree;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.tree.Tree;
import com.jk.service.tree.TreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController()
@RequestMapping("tree")
public class TreeController {
    @Reference
    private TreeService treeService;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @RequestMapping("findAllTree")
   /* @Cacheable(value = "redis",key = "01")*/
    public List<Tree> selectAllTree(){
       return treeService.selectAllTree();
    }
}
