package com.jk.controller.goodstype;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.brand.Brand;
import com.jk.model.color.Color;
import com.jk.model.goods.Goods;

import com.jk.service.goodstype.GoodsTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("pagetype")
public class GoodsTypeController {
    @Reference
    private GoodsTypeService goodsTypeService;

    @RequestMapping("topage")
    public String topage(){
        return "view/goodspage/list";
    }

    @RequestMapping("findtypelist")
    @ResponseBody
    public List<Goods> selecttypelist(Long id,String brand,String color){
        if(brand.length()==0&&color.length()==0){
            List<Goods> goodtypelist =  goodsTypeService.selecttypelist(id,brand,color);
            return goodtypelist;
        }else if(brand.length()==0){
            String cvalue =  color.substring(0,color.length()-1);
            List<Goods> goodtypelist =  goodsTypeService.selecttypelist(id,brand,cvalue);
            return goodtypelist;
        }else if(color.length()==0){
            String bvalue =  brand.substring(0,brand.length()-1);
            List<Goods> goodtypelist =  goodsTypeService.selecttypelist(id,bvalue,color);
            return goodtypelist;
        }else{
            String cvalue =  color.substring(0,color.length()-1);
            String bvalue =  brand.substring(0,brand.length()-1);
            List<Goods> goodtypelist =  goodsTypeService.selecttypelist(id,bvalue,cvalue);
            return goodtypelist;
        }



    }

    @RequestMapping("findbrandlist")
    @ResponseBody
    public List<Brand> selectbrandlist(){
        List<Brand> goodbrandlist =  goodsTypeService.selectbrandlist();
        return goodbrandlist;
    }

    @RequestMapping("findcolorlist")
    @ResponseBody
    public List<Color> selectcolorlist(){
        List<Color> goodcolorlist =  goodsTypeService.selectcolorlist();
        return goodcolorlist;
    }

    @RequestMapping("findByPrice")
    @ResponseBody
    public List<Goods> selecByPrice(){
        List<Goods> pricelist =  goodsTypeService.selecByPrice();
        return pricelist;
    }






}
