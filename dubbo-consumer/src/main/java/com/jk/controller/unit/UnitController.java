package com.jk.controller.unit;


import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.unit.Unit;
import com.jk.service.unit.UnitService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

@Controller
@RequestMapping("unit")
public class UnitController {
    @Reference(timeout = 120000)
    private UnitService unitService;

    @RequestMapping("queryUnitList")
    @ResponseBody
    public List<Unit> selectUnitList(Unit unit){

        List<Unit> unitList = unitService.selectUnitList(unit);

        return unitList;
    }
    @RequestMapping("queryUnitPid")
    @ResponseBody
    public String queryUnitPid(){
        return unitService.queryUnitPid();
    }
    @RequestMapping("addUnit")
    @ResponseBody
    public void saveUnit(Unit unit){
        unitService.saveUnit(unit);
    }
    @RequestMapping("deleteUnit")
    @ResponseBody
    public String deleteUnit(Long unitId){
        unitService.deleteUnit(unitId);
        return "1";
    }
}
