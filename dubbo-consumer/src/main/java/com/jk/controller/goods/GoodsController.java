package com.jk.controller.goods;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.brand.Brand;
import com.jk.model.center.GoodsCenter;
import com.jk.model.color.Color;
import com.jk.model.goods.Goods;
import com.jk.model.product.Product;
import com.jk.model.type.Type;
import com.jk.model.unit.Unit;
import com.jk.service.Product.ProductService;
import com.jk.service.brand.BrandService;
import com.jk.service.color.GoodsColorService;
import com.jk.service.goods.GoodsService;
import com.jk.service.type.TypeService;
import com.jk.service.unit.UnitService;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("goods")
public class GoodsController {

    @Reference
    private GoodsService goodsService;

    @Reference
    private ProductService productService;

    @Reference
    private BrandService brandService;

    @Reference
    private TypeService typeService;

    @Reference
    private GoodsColorService goodsColorServiceice;

    @Reference
    private UnitService unitService;

      @RequestMapping("zhuce")
      public String zhuce(){
          return "view/zhuce";
      }
    //折现图
    @RequestMapping("queryBrokenLine")
    @ResponseBody
    public Map<String,Object> queryBrokenLine(){
        return goodsService.queryBrokenLine();
    }
    //饼状图查询
    @RequestMapping("queryReportBin")
    @ResponseBody
    public List<Map<String,Object>> queryReportBin(){
        return goodsService.queryReportBin();
    }

    /*
    * 跳转饼状图页面
    * */
    @RequestMapping("bingz")
    public String bingz(){
        return "view/echart/bingz";
    }
    /*
    * 跳转折现图页面
    * */
    @RequestMapping("tozhexian")
    public String tozhexian(){
        return "view/echart/tozhexian";
    }
    /*
    * 跳转柱状图页面
    * */
    @RequestMapping("tozhuzhuang")
    public String tozhuzhuang(){
        return "view/echart/tozhuzhuang";
    }

    @RequestMapping("queryUnit")
    @ResponseBody
    public List<Unit> selectUnitList(Unit unit){
        List<Unit> unitList = unitService.selectUnitList(unit);
        return unitList;
    }

    @RequestMapping("insertGoods")
    @ResponseBody
    public String insertGoods(Goods goods){
        goodsService.insertGoods(goods);
        return "1";
    }
    @RequestMapping("queryColor")
    @ResponseBody
    public List<Color>  selectColor(){
        List<Color> color = goodsColorServiceice.seleGoodsColorList();
        return color;
    }
    @RequestMapping("selectproduct")
    @ResponseBody
    public  List<Product> selectproduct(HttpSession session,Product product){
        List<Product> productList = productService.findAllProduct(product);
        return productList;
    }
    @RequestMapping("queryType")
    @ResponseBody
    public List<Type> selectTypeTree(){
        List<Type> type=goodsService.selectTypeTree();
        return type;
    }
    @RequestMapping("querygsize")
    @ResponseBody
    public List<GoodsCenter> querygsize(){
        List<GoodsCenter> goodsCenter=goodsService.querygsize();
        return goodsCenter;
    }
    @RequestMapping("queryBrand")
    @ResponseBody
    public List<Brand>  selectBrand(Brand brand){
        List<Brand> list= brandService.selectBrandList(brand);
        return list;
    }


    @RequestMapping("selectGoodsById")
    public String selectGoodsById(Long gid, Model model){
        Goods goods=goodsService.selectGoodsById(gid);
        model.addAttribute("goods",goods);
        return "view/goods/editGoods";
    }
    /*
    * 批量品牌
    * */
    @RequestMapping("updateBrandAll")
    @ResponseBody
    public  String updateBrandAll(Long bid,Long[] ids){
        goodsService.updateBrandAll(bid,ids);
        return "1";
    }
    /*
    * 批量分类
    * */
    @RequestMapping("updateTypeAll")
    @ResponseBody
    public  String updateTypeAll(Long tid,Long[] ids){
        goodsService.updateTypeAll(tid,ids);
        return "1";
    }

    /*
    * 跳转批量品牌页面
    * */
    @RequestMapping("toUpBeandAll")
    public String toUpBeandAll(){
        return "view/goods/toUpBread";
    }
    /*
    * 跳转类型品牌页面
    * */
    @RequestMapping("toUpTypeAll")
    public String toUpTypeAll(){
        return "view/goods/toUpTypeAll";
    }

    @RequestMapping("togoods")
    public String togoods(){
        return "view/goods/queryGoods";
    }


    @RequestMapping("deleteCompary")
    @ResponseBody
    public String deleteCompary(Long gid){
        goodsService.deleteCompary(gid);
        return "1";
    }

    @RequestMapping("selectGoods")
    @ResponseBody
    public List<Goods> selectGoods(){
        return goodsService.selectGoods();
    }



}
