package com.jk.utils;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.*;
import com.jk.oss.ConstantProperties;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by lightClouds917
 * Date 2018/2/7
 * Description:aliyunOSSUtil
 */
public class AliyunOSSUtil {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AliyunOSSUtil.class);
    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 上传文件
     *
     * @param file
     * @return
     */
    public static String upload(File file) {
        logger.info("=========>OSS文件上传开始：" + file.getName());
        String endpoint = ConstantProperties.JAVA4ALL_END_POINT;
        String accessKeyId = ConstantProperties.JAVA4ALL_ACCESS_KEY_ID;
        String accessKeySecret = ConstantProperties.JAVA4ALL_ACCESS_KEY_SECRET;
        String bucketName = ConstantProperties.JAVA4ALL_BUCKET_NAME1;
        String fileHost = ConstantProperties.JAVA4ALL_FILE_HOST;
        String dateStr = format.format(new Date());
        if (null == file) {
            return null;
        }
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        try {
            //容器不存在，就创建
            if (!ossClient.doesBucketExist(bucketName)) {
                ossClient.createBucket(bucketName);
                CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
                createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
                ossClient.createBucket(createBucketRequest);
            }
            //创建文件路径
            String fileUrl = fileHost + "/" + (dateStr + "/" + UUID.randomUUID().toString().replace("-", "") + "-" + file.getName());
            //上传文件
            PutObjectResult result = ossClient.putObject(new PutObjectRequest(bucketName, fileUrl, file));
            //设置权限 这里是公开读
            ossClient.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);
            if (null != result) {
                logger.info("==========>OSS文件上传成功,OSS地址：" + "https://"+bucketName+"."+endpoint+"/"+fileUrl);
                return "https://"+bucketName+"."+endpoint+"/"+fileUrl;
            }
        } catch (OSSException oe) {
            logger.error(oe.getMessage());
        } catch (ClientException ce) {
            logger.error(ce.getMessage());
        } finally {
            //关闭
            ossClient.shutdown();
        }
        return null;
    }
    /**
     * 从阿里云下载文件 （以附件形式下载）
     * @param request
     * @param response
     */
    public static void download(HttpServletRequest request, HttpServletResponse response){
        try {
            String filename =request.getParameter("filename").toString();
           /* int i=filename.lastIndexOf("//");*/
            filename=filename.substring(46);

            String endpoint = ConstantProperties.JAVA4ALL_END_POINT;
            String accessKeyId = ConstantProperties.JAVA4ALL_ACCESS_KEY_ID;
            String accessKeySecret = ConstantProperties.JAVA4ALL_ACCESS_KEY_SECRET;
            String bucketName = ConstantProperties.JAVA4ALL_BUCKET_NAME1;
            String fileHost = ConstantProperties.JAVA4ALL_FILE_HOST;
            OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
            //获取fileid对应的阿里云上的文件对象
            OSSObject ossObject = ossClient.getObject(bucketName, filename);//bucketName需要自己设置
            // 读去Object内容  返回
            BufferedInputStream in=new BufferedInputStream(ossObject.getObjectContent());
            //System.out.println(ossObject.getObjectContent().toString());
            BufferedOutputStream out=new BufferedOutputStream(response.getOutputStream());
            //通知浏览器以附件形式下载
            response.setHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode(filename,"utf-8"));
            //BufferedOutputStream out=new BufferedOutputStream(new FileOutputStream(new File("f:\\a.txt")));
            byte[] car=new byte[1024];
            int L=0;
            while((L=in.read(car))!=-1){
                out.write(car, 0,L);
            }
            if(out!=null){
                out.flush();
                out.close();
            }
            if(in!=null){
                in.close();
            }

            ossClient.shutdown();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //    下载
    public static void  download1(String name) {
        String filename = "blog/2019-01-03/57a5319a558641f98d64f7e9a52bfa91-9.jpg";
        logger.info("=========>OSS文件下载开始：");
        String endpoint = ConstantProperties.JAVA4ALL_END_POINT;
        String accessKeyId = ConstantProperties.JAVA4ALL_ACCESS_KEY_ID;
        String accessKeySecret = ConstantProperties.JAVA4ALL_ACCESS_KEY_SECRET;
        String bucketName = ConstantProperties.JAVA4ALL_BUCKET_NAME1;
        String fileHost = ConstantProperties.JAVA4ALL_FILE_HOST;
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        OSSObject object = ossClient.getObject(bucketName, filename);
        // 获取ObjectMeta
       /* ObjectMetadata meta = object.getObjectMetadata();*/
        // 获取Object的输入流
         InputStream objectContent = object.getObjectContent();
        ObjectMetadata objectData = ossClient.getObject(new GetObjectRequest(bucketName, filename), new File("123"));
        // 关闭数据流  objectContent.close();
        ossClient.shutdown();
    }


    /**
     * 删除Object
     * @param fileKey
     * @return
     */
    public static String deleteBlog(String fileKey){
        logger.info("=========>OSS文件删除开始");
        String endpoint=ConstantProperties.JAVA4ALL_END_POINT;
        String accessKeyId=ConstantProperties.JAVA4ALL_ACCESS_KEY_ID;
        String accessKeySecret=ConstantProperties.JAVA4ALL_ACCESS_KEY_SECRET;
        String bucketName=ConstantProperties.JAVA4ALL_BUCKET_NAME1;
        String fileHost=ConstantProperties.JAVA4ALL_FILE_HOST;
        try {
            OSSClient ossClient = new OSSClient(endpoint,accessKeyId,accessKeySecret);

            if(!ossClient.doesBucketExist(bucketName)){
                logger.info("==============>您的Bucket不存在");
                return "您的Bucket不存在";
            }else {
                logger.info("==============>开始删除Object");
                ossClient.deleteObject(bucketName,fileKey);
                logger.info("==============>Object删除成功："+fileKey);
                return "==============>Object删除成功："+fileKey;
            }
        }catch (Exception ex){
            logger.info("删除Object失败",ex);
            return "删除Object失败";
        }
    }

    /**
     * 查询文件名列表
     * @param bucketName
     * @return
     */
    public static List<String> getObjectList(String bucketName){
        List<String> listRe = new ArrayList<>();
        String endpoint=ConstantProperties.JAVA4ALL_END_POINT;
        String accessKeyId=ConstantProperties.JAVA4ALL_ACCESS_KEY_ID;
        String accessKeySecret=ConstantProperties.JAVA4ALL_ACCESS_KEY_SECRET;
        try {
            logger.info("===========>查询文件名列表");
            OSSClient ossClient = new OSSClient(endpoint,accessKeyId,accessKeySecret);
            ListObjectsRequest listObjectsRequest = new ListObjectsRequest(bucketName);
            //列出blog目录下今天所有文件
            listObjectsRequest.setPrefix("blog/"+format.format(new Date())+"/");
            ObjectListing list = ossClient.listObjects(listObjectsRequest);
            for(OSSObjectSummary objectSummary : list.getObjectSummaries()){
                listRe.add(objectSummary.getKey());
            }
            return listRe;
        }catch (Exception ex){
            logger.info("==========>查询列表失败",ex);
            return new ArrayList<>();
        }
    }
}
