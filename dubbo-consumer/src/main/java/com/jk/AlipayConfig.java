package com.jk;

public class AlipayConfig {
    //开发者中心 / 研发服务 / 沙箱环境 / 沙箱应用/ 信息配置/ 必看部分/ APPID（填自己的，下面都是改过的~）
     public static String app_id = "2016092400586404";
    // 开发者中心 / 研发服务 / 沙箱环境 / 沙箱应用/ 信息配置/ 必看部分/ 生成公钥时对应的私钥（填自己的，下面都是改过的~）
     public static String private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC9SqCGZEA5pr1v\n" +
            "3PEJOtYBE7TlFvDRl1CLc1SlspXdD4V/plZzhp9CBteBpIuj3fj0OZlM4yqHc1tj\n" +
            "kujQWoJh8sM5mzXkt9BPsWU8V9cChVMV0ZtM8ba6YzCu1RrUECSfdcJS0YXZvDtw\n" +
            "RCo4nGTcyyZROohYIDpOa3gNimBbqUUmJU7nJ1fJ3zwzE/re8JU5OO3PCYRJ/4MD\n" +
            "EuCPbi+RJb+chFyi42vqW73deCaiLmBFzzJ2OUJsOTfpbpzbOK5PksPIDL9sxwGz\n" +
            "xbMF+Rn8Kzlvt1d2u3hW9aeSSEgVux9ZjLbWcnBSwYaYHMesyvwMtxCHDGhaqfAG\n" +
            "dTusxsvhAgMBAAECggEBALgw1NDQ3jn7CNy/1f+QM8tqfJxq3TxlC+gV+mkdxHwN\n" +
            "7W5P0lGpznA2Lh+lmIeEqAolvgk2Bx00X9oditOrLrHWwqbcY+3B+z6AtKyqILsA\n" +
            "Nt/FTWPq1E671BRbozZ02t4bfVKPNLqxUfklKxxY+kBqoUdrOF7gRTwnkPNEEg09\n" +
            "lWsHwL9eMDbP9XQdAdBPMqtR5SSN2sqJlJRdob1zH2w0HkqPZjuNUw/acjxkm7dk\n" +
            "f/1Xc+z625g5XWOeAjpvHtYgcGpfaWjNX7yFd85ZIROTEvq6+VrbTrezDAUyHx4R\n" +
            "qMTTjZyVzgk0EmNP9Us2l8ZNumshUmS73bfD/2SsGp0CgYEA30Zh9VsGZdHjpNPl\n" +
            "anunYGCfHSJ5JWo+JpfMAoY0qa44z9F1+DC4k/yS0m2QCcP1F3PTAICJ9jUDi0Pq\n" +
            "nmN8fI+faUZmV+W0+wwhlz6MC5vSWGZfnmDzexdyL0/dBj/jMk1pm+NpCXNbArUa\n" +
            "aMDeLitJCoBBpu1OU/vg01l4FasCgYEA2QkiajLPMcC+k2703KGiDGQqfKfY2kIJ\n" +
            "mVlqg7NSZohue3RGYrMtgKXYbDwQ2Z+aU36907FTNLYTZbVzdqboEfuv7yOXFBSG\n" +
            "gjWYnjsDB/VlWXXNjWCbNfJrLASGYo4+gXLOpTMRc8E1PpjoH1hs+pjU/jswQ8Ll\n" +
            "Fm/Om2zzAKMCgYBIjWZnhBygGYQPm4uA/o11E/CgVwNm1Q89GzZ/23xhZuuvHDFp\n" +
            "ZOuVqw1Ks6+KfNK3vDtkfmBWUQPFzVv/+/QHxePXIbk/9rTInziLHRgE7a18bEfF\n" +
            "/1sfNI82MI1x6nC5d85JiawoFgQM7SyXvZR1DDu/3c13oMVDJSX5h6kt4QKBgQDP\n" +
            "riX4aVZKzZ2PhwVYenKOWOsvDY48jCSTUTTTgay/4J2YFNOlbr+D8y79oxycolUS\n" +
            "wJz1DSIJtpEpKlCqoVxAmQ7pTq0q0dlEj/TStLVZ0ECDMm7XcjITlT1oiYvukx7M\n" +
            "0AddnVh948wHclnsZLkjs0tdW1X8AWsl5SdXIxIE8QKBgH037e1MGmNZTn3TV2IH\n" +
            "Z+NXtqaR1zi9tGvWTWPL/FZMN3jMsLSjaATJAoTrUkjdyziWqTRpCHbS5TloIWCz\n" +
            "ygU2T0Ibzh/VFjlxJ4ZLi5WPqNyjT9nQWEuAIE6PjOnsMytBDUrc0UJyiK0e1dFx\n" +
            "E2IHVeXhRyC6QeFKe6EeiWNg\n";
    // Controller Mapping:得放到服务器上，且使用域名解析 IP
     public static String notify_url = "http://www.linhongcun.com/SpringBootPay-0.0.1-SNAPSHOT/pay/notifyUrl";
    // Controller Mapping:得放到服务器上，且使用域名解析 IP
     public static String return_url = "http://localhost:8081/goodsDetails/seleColor?gid=2";
    // 开发者中心 / 研发服务 / 沙箱环境 / 沙箱应用/ 信息配置/ 必看部分/ 支付宝网关（注意沙箱alipaydev，正式则为 alipay）
     public static String url = "https://openapi.alipaydev.com/gateway.do";
     public static String charset = "UTF-8";
     public static String format = "json";
    // 开发者中心 / 研发服务 / 沙箱环境 / 沙箱应用/ 信息配置/ 必看部分/ 公钥（填自己的，下面都是改过的~）
     public static String public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2Z5VHTf0bEbuhLKmpdzocnyDF4XYF5o4LYG+hGXfEnIRWWntvVYmyA9qjah81SqlQmQRe3pSsAqvjI2ARibE4KURbUSgNOmR+YMGRUP+lJUjUSOwNvmxKG/tsOpWYcazSy71w6CHMItt99WJNOXnewNxwYFIAOIcSVEpfFP3KDn5+jXyA6nCHYkkY4pe4waspMUPG15GqQ3kKlgmyN6twLwHFrjxZrLG8mfyBcHT1x/M4Cqb0Uy1XPw8klM8XGocji4sNqI/QHtY8fBl08Tt+3AN9tbLZ/Pgk6QkqOybLW8+Pd1h7ifUbm3yNrXA9KToaPPKBzjvgcgiPO5999Mc6QIDAQAB";
     public static String signtype = "RSA2";

}
