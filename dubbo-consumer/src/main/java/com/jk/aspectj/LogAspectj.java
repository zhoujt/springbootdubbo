/** 
 * <pre>项目名称:maven_easyui_user 
 * 文件名称:LogAspectj.java 
 * 包名:com.jk.zjt.aspectj 
 * 创建日期:2018年11月22日下午3:18:37 
 * Copyright (c) 2018, yuxy123@gmail.com All Rights Reserved.</pre> 
 */  
package com.jk.aspectj;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import com.alibaba.fastjson.JSON;
import com.jk.model.log.Log;
import com.jk.model.user.User;
import com.jk.utils.StringUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


@Component
@Aspect
public class LogAspectj {
@Autowired
private MongoTemplate mongoTemplate;
@Autowired
private AmqpTemplate amqpTemplate;
@Pointcut("execution(* com.jk.controller.*.*.*(..))")
public void logPointCut() {}

@AfterReturning(value="logPointCut()",argNames="rtv",returning="rtv")
public void saveLogAdvice(JoinPoint joinPoint, Object rtv) {
	Object[] args = joinPoint.getArgs();
	if(args == null) {
		return ;
	}else {
		Log log = new Log();
		log.setCreateTime(new Date());
		String classname = joinPoint.getTarget().getClass().getSimpleName();
		log.setClassName(classname);
		
		String name = joinPoint.getSignature().getName();
		log.setMethod(name);
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("参数{");
		for (Object param : args) {
			stringBuffer.append(param+",");
		}
		stringBuffer.append("}");
		log.setReqParam(stringBuffer.toString());
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		ServletRequestAttributes sra = (ServletRequestAttributes) ra;
		if(sra != null) {
			HttpServletRequest request = sra.getRequest();
			HttpSession session = request.getSession();
			 User user = (User) session.getAttribute("sessionUser");
			if(user != null) {
				log.setUserName(user.getName());
			}
			
			log.setUrl(request.getRequestURI().toString());
			log.setIp(getIp(request));
		}
		String logstring = JSON.toJSONString(log);
		amqpTemplate.convertAndSend("orderQueue1",logstring);
		/*mongoTemplate.save(log);*/
	}
}


private String getIp(HttpServletRequest request) {
	String ip = request.getHeader("X-Forwarded-For");
	if(StringUtil.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)) {
	int index = ip.indexOf(",");
	if(index != -1) {
		return ip.substring(0,index);
	}else {
		return ip;
	}
}
	ip = request.getHeader("X-Real-IP");  
	if(StringUtil.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
	    return ip;  
	}  
	return request.getRemoteAddr();
	}
//前置通知: 在调用切点方法之前，执行的通知
	@Before("logPointCut()")
	public void beforeAdvice(){
	    System.out.println("注解类型前置通知");
	}

/*

//前置通知: 在调用切点方法之前，执行的通知
	@Before("logPointCut()")
	public void beforeAdvice(){
	    System.out.println("注解类型前置通知");
	}
	
	//后置通知: 在调用切点方法之后，执行的通知
	@After("logPointCut()")
	public void afterAdvice(){
	    System.out.println("注解类型后置通知");
	}
	
	//环绕通知。注意要有ProceedingJoinPoint参数传入。
	// 调用切点方法之前调用，然后调用proceed()方法去执行切点方法，再进入环绕通知
	@Around("logPointCut()")
	public void sayAround(ProceedingJoinPoint pjp) throws Throwable{
	    System.out.println("注解类型环绕通知..环绕前");
	    pjp.proceed();//执行方法
	    System.out.println("注解类型环绕通知..环绕后");
	}*/
}
