package com.jk.service.user;

import com.jk.model.user.User;

import java.util.List;

public interface UserService {
    List<User> selectAllUser();

    int saveUser(User user);

    User findUserById(Integer id);

    void deleteUserById(Integer id);

    int updateUser(User user);

    User findUserByUsername(String principal);

    List<String> queryPowerKeyList(Long id);

    User selectUserByPhone(String phone);
}
