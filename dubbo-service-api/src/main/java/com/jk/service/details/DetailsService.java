package com.jk.service.details;

import com.jk.model.order.Order;
import com.jk.model.center.GoodsCenter;
import com.jk.model.goods.Goods;

import java.util.List;

public interface DetailsService {

    List<Goods> seleGoodsDetails(Long gid);

    List<GoodsCenter> seleColor(Long gid);

    void addList(Order order);

    List<Order> queryOrder(Long userId);

    void deleteOrder(Long rid);
}
