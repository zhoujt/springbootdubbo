package com.jk.service.Product;

import com.jk.model.brand.Brand;
import com.jk.model.color.Color;
import com.jk.model.goods.Goods;
import com.jk.model.product.Middle;
import com.jk.model.product.Product;
import com.jk.model.product.Treet;
import com.jk.model.type.Type;
import com.jk.model.unit.Unit;
import com.jk.model.user.User;

import java.util.List;

public interface ProductService {
    List<Product> findAllProduct(Product product);


    void addProduct(Product product);

    void deleteProduct(long pid);


    void edit(Product product);

    List<Brand> brandlist();

    List<Type> typelist();

    Product queryproductOrderById(Long pid);

    void addinsertProduct(Goods goods);

    List<Unit> unittypelist();

    List<Product> goosderById(Long pid);

   List<Treet> totreelist(Integer id);

    List<Color> colortypelist();

    void tomiddle(Goods goods);

    List<Goods> centerlist();

    void tomiddlecenter(Middle middle);

    User getUserListt2(String loginName);

    void toUser(User user);




    /* Product queryproductOrderById(Product product);*/
}
