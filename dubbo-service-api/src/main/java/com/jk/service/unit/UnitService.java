package com.jk.service.unit;

import com.jk.model.unit.Unit;

import java.util.List;

public interface UnitService {
    List<Unit> selectUnitList(Unit unit);

    String queryUnitPid();

    void saveUnit(Unit unit);

    void deleteUnit(Long unitId);
}
