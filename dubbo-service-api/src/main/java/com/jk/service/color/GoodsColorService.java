package com.jk.service.color;

import com.jk.model.color.Color;

import java.util.List;

public interface GoodsColorService {

    List<Color> seleGoodsColorList();


    void deleteById(Integer id);

    List<Color> seleListPid();

    void addList(Color color);
}
