package com.jk.service.homepage;

import com.jk.model.type.Type;

import java.util.List;

public interface HomeService {
    List<Type> selectTypeByPid();

    List<Type> selectTypeById();
}
