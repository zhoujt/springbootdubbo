package com.jk.service.poi;

import com.jk.model.goods.Goods;

import java.util.List;

public interface PoiService {
    void addGoods(List<Goods> list);
}
