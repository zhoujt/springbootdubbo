package com.jk.service.goods;

import com.jk.model.center.GoodsCenter;
import com.jk.model.goods.Goods;
import com.jk.model.type.Type;

import java.io.File;
import java.util.List;
import java.util.Map;


public interface GoodsService {


    List<Goods> selectGoods();

    Goods selectGoodsById(Long gid);

    List<Goods> selectColor();

    List<Goods> selectBrand();

    List<Goods> selectproduct();

    List<Type> selectTypeTree();

    void insertGoods(Goods goods);

    void deleteCompary(Long gid);

    void updateBrandAll(Long bid,Long[] ids);

    void updateTypeAll(Long tid, Long[] ids);

    void uploadBlog(String uploadUrl,Long gid);

    List<GoodsCenter> querygsize();

    List<Map<String, Object>> queryReportBin();

    Map<String, Object> queryBrokenLine();
}
