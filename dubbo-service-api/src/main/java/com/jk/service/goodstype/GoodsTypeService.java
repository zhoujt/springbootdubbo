package com.jk.service.goodstype;

import com.jk.model.brand.Brand;
import com.jk.model.color.Color;
import com.jk.model.goods.Goods;

import java.util.List;

public interface GoodsTypeService {
    List<Goods> selecttypelist(Long id,String bvalue,String cvalue);

    List<Brand> selectbrandlist();



    List<Color> selectcolorlist();

    List<Goods> selecByPrice();
}
