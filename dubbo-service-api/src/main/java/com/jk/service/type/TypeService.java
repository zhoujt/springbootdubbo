package com.jk.service.type;


import com.jk.model.type.Type;

import java.util.List;

public interface TypeService {
    List<Type> selectTypeTree();

    void addTypeByPid(Type type);

    void addById(Type type);
}
