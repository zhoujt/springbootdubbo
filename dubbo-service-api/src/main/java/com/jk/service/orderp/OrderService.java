package com.jk.service.orderp;

import com.jk.model.order.Order;

import java.util.List;

public interface OrderService {
    List<Order> queryOrderList(Long userId);

    void deleteOrder(long rid);
}
