package com.jk;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "com.jk.*")
@SpringBootApplication
@EnableDubboConfiguration


@MapperScan("com.jk.mapper")
public class DubboProviderApplication {
    public static void main(String[] args)   {
        SpringApplication.run(DubboProviderApplication.class, args);
    }

}

