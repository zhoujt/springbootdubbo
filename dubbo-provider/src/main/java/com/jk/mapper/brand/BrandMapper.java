package com.jk.mapper.brand;

import com.jk.model.brand.Brand;

import java.util.List;

public interface BrandMapper {
    List<Brand> selectBrandList(Brand brand);

    void saveBrand(Brand brand);

    Brand selectBrandById(Long bid);

    void removeBrand(Long bid);

    void updateBrand(Brand brand);
}
