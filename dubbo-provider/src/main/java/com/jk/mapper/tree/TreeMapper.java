package com.jk.mapper.tree;

import com.jk.model.tree.Tree;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface TreeMapper {
    List<Tree> selectAllTree();

    List<Tree> queryTreeById();
}
