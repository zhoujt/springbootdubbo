package com.jk.mapper.type;

import com.jk.model.type.Type;

import java.util.List;

public interface TypeMapper {
    List<Type> queryTypeById();

    void addTypeByPid(Type type);

    void addById(Type type);
}
