package com.jk.mapper.homepage;

import com.jk.model.type.Type;

import java.util.List;

public interface HomeMapper {
    List<Type> selectTypeByPid();

    List<Type> selectTypeById();
}
