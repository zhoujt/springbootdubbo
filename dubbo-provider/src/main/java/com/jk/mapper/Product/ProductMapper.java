package com.jk.mapper.Product;

import com.jk.model.brand.Brand;
import com.jk.model.color.Color;
import com.jk.model.goods.Goods;
import com.jk.model.product.Middle;
import com.jk.model.product.Product;
import com.jk.model.product.Treet;
import com.jk.model.type.Type;
import com.jk.model.unit.Unit;
import com.jk.model.user.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface ProductMapper {

   /* @Select("select p_id as pId,p_name as pName,p_status as pStatus from cz_product")*/
    List<Product> findAllProduct(Product product);


    void addProduct(Product product);

    void deleteProduct(long pid);


    void edit(Product product);

    Product queryproductOrderById(Long pid);

    List<Brand> brandlist();

    List<Type> typelist();

    void addinsertProduct(Goods goods);

    List<Unit> unittypelist();

    List<Product> goosderById(Long pid);

   

    List<Color> colortypelist();

    List<Treet> getTreeFs(Integer pid);

    void tomiddle(Goods goods);

    List<Goods> centerlist();

    void tomiddlecenter(Middle middle);


    User getUserListt2(String loginName);

     void toUser(User user);




 /* Product queryproductOrderById(Product product);*/
}
