package com.jk.mapper.goodstype;

import com.jk.model.brand.Brand;
import com.jk.model.color.Color;
import com.jk.model.goods.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface GoodsTypeMapper {
    List<Goods> selecttypelist(@Param("id")Long id, @Param("bvalue")String bvalue,@Param("cvalue")String cvalue);

    List<Brand> selectbrandlist();



    List<Color> selectcolorlist();

    List<Goods> selecByPrice();
}
