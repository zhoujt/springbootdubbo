package com.jk.mapper.details;

import com.jk.model.order.Order;
import com.jk.model.center.GoodsCenter;
import com.jk.model.goods.Goods;

import java.util.List;

public interface DetailsMapper {

    List<Goods> seleGoodsDetails(Long gid);

    List<GoodsCenter> seleColor(Long gid);

    void addList(Order order);

    List<Order> queryOrder(Long userId);

    void deleteOrder(Long rid);
}
