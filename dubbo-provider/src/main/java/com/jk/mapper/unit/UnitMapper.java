package com.jk.mapper.unit;

import com.jk.model.unit.Unit;
import java.util.List;

public interface UnitMapper {
    List<Unit> selectUnitList(Unit unit);

    String queryUnitPid();

    void saveUnit(Unit unit);

    void deleteUnit(Long unitId);
}
