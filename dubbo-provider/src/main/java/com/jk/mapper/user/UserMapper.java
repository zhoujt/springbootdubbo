package com.jk.mapper.user;

import com.jk.mapper.user.UserProvider;
import com.jk.model.user.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface UserMapper {
  /*  @Results(id = "userMap",value = {
            @Result(property = "loginName",column = "login_name"),
            @Result(property = "id",column = "id"),
            @Result(property = "name",column = "name"),
            @Result(property = "password",column = "password"),
            @Result(column="sex",property="sex"),
            @Result(column="age",property="age"),
            @Result(property = "phone",column = "phone"),
            @Result(column="user_type",property="userType"),
            @Result(column="status",property="status"),
            @Result(column="del_flag",property="delFlag"),
            @Result(property="updateTime",column="update_time"),
            @Result(property="createTime",column="create_time"),
            @Result(property="userImg",column="user_img"),
            @Result(property="birthday",column="birthday"),
            @Result(property="deptId",column="dept_id"),
            @Result(property="deptName",column="dept_name"),
            @Result(property="roleNames",column="role_names"),
            @Result(property="resourceNames",column="resource_names")
    })*/
//    @Select("select * from springboottest.sys_user")
    /*@SelectProvider(type = UserProvider.class,method = "selectUserWithCondition")*/

    @Insert("insert into sys_user(login_name,name,password,phone) values (#{loginName},#{name},#{password},#{phone})")
    int saveUser(User user);
  /*  @ResultMap("userMap")*/
    @Select("select * from sys_user where id = #{id}")
    User findUserById(@Param("id") Integer id);
    @Delete("delete from sys_user where id = #{id}")
    void deleteUserById(@Param("id") Integer id);
    @Update("update sys_user set login_name = #{user.loginName},name=#{user.name},password=#{user.password},phone=#{user.phone} where id = #{user.id}")
    int updateUser(@Param("user") User user);

    User findUserByUsername(String principal);

    List<String> queryPowerKeyList(Long id);

    List<User> selectAllUser();

    @Select("select * from sys_user where phone = #{phone}")
    User selectUserByPhone(String phone);
}
