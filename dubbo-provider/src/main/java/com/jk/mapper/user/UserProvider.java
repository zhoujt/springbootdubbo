package com.jk.mapper.user;

import com.jk.model.user.User;
import org.apache.ibatis.jdbc.SQL;

public class UserProvider {
    public String selectUserWithCondition(User user){
        SQL sql = new SQL().SELECT("*").FROM("sys_user");
        if (user != null){
            if (user.getName() != null && !"".equals(user.getName())){
                sql.WHERE("name like '%"+user.getName()+"%'");
            }
        }
        return sql.toString();
    }
}
