package com.jk.mapper.color;

import com.jk.model.color.Color;

import java.util.List;

public interface GoodsColorMapper {

    List<Color> seleGoodsColorList();

    void deleteById(Integer id);

    List<Color> seleListPid();

    void addList(Color color);
}
