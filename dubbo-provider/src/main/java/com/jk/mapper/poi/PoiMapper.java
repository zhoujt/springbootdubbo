package com.jk.mapper.poi;

import com.jk.model.goods.Goods;

import java.util.List;

public interface PoiMapper {
    void addGoods(List<Goods> list);
}
