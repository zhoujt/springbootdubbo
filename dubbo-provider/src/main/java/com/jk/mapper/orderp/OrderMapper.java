package com.jk.mapper.orderp;

import com.jk.model.order.Order;

import java.util.List;

public interface OrderMapper {
    List<Order> queryOrderList(Long userId);

    void deleteOrder(long rid);
}
