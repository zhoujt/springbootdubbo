/** 
 * <pre>项目名称:secondhand-car-consumer 
 * 文件名称:MongoDBConfig.java 
 * 包名:com.jk 
 * 创建日期:2018年9月18日下午11:51:52 
 * Copyright (c) 2018, lxm_man@163.com All Rights Reserved.</pre> 
 */  
package com.jk;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MongoDBConfig {
	private final static Logger logger = LoggerFactory.getLogger(MongoDBConfig.class);

    @Autowired
    private MongoDBProperties mongoDBProperties;

    @SuppressWarnings("deprecation")
	@Bean
    //覆盖
    @ConditionalOnMissingBean(MongoClient.class)
    public MongoClient getMongodbClients() {
        String host = mongoDBProperties.getHost();
        if(StringUtils.isEmpty(host)){
            throw new RuntimeException("MongoDB host is empty.");
        }
        String[] hosts = host.split(",");
        String[] address;
        List<ServerAddress> addresses = new ArrayList<ServerAddress>();
        for( String h : hosts ){
            if( StringUtils.isEmpty(h) ){
                logger.warn("This MongoDB host [{}] is empty, continue init this host.", h);
                continue;
            }

            if( h.indexOf(":") != -1 ){
                address = h.split(":");
                logger.info("Init MongoDB : host [{}], port [{}]", address[0], address[1]);
                addresses.add(new ServerAddress(address[0], Integer.valueOf(address[1])));
            }else{
                logger.info("Init MongoDB : host [{}], no port.", h);
                addresses.add(new ServerAddress(h));
            }
        }
        
       
        
        MongoClient client = new MongoClient(addresses);
   
        return client;
    }

}
