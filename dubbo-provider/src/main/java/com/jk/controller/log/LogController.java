package com.jk.controller.log;

import com.alibaba.fastjson.JSON;
import com.jk.model.log.Log;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class LogController {
    @Autowired
    private MongoTemplate mongoTemplate;
    @RabbitListener(queues = "orderQueue1")
    public void recieveMessage(String logmessage){
        Log log = JSON.parseObject(logmessage, Log.class);
        mongoTemplate.save(log);
    }
}
