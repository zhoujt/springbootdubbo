package com.jk.service.details;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.details.DetailsMapper;
import com.jk.model.order.Order;
import com.jk.model.center.GoodsCenter;
import com.jk.model.goods.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = DetailsService.class)
@Component
public class DetailsServiceImpl implements DetailsService{
    @Autowired
    private DetailsMapper detailsMapper;
    //查询详情页面
    @Override
    public List<Goods> seleGoodsDetails(Long gid) {
        return detailsMapper.seleGoodsDetails(gid);
    }

    @Override
    public List<GoodsCenter> seleColor(Long gid) {
        return detailsMapper.seleColor(gid);
    }

    @Override
    public void addList(Order order) {
        detailsMapper.addList(order);
    }

    @Override
    public List<Order> queryOrder(Long userId) {
        return detailsMapper.queryOrder(userId);
    }

    @Override
    public void deleteOrder(Long rid) {
        detailsMapper.deleteOrder(rid);
    }


}
