package com.jk.service.poi;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.poi.PoiMapper;
import com.jk.model.goods.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = PoiService.class)
@Component
public class PoiServiceImpl implements PoiService{
    @Autowired
    private PoiMapper poiMapper;

    @Override
    public void addGoods(List<Goods> list) {
        poiMapper.addGoods(list);
    }
}
