package com.jk.service.orderp;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.orderp.OrderMapper;
import com.jk.model.order.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
@Service(interfaceClass=OrderService.class)
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderMapper orderMapper;

    @Override
    public List<Order> queryOrderList(Long userId) {
        return orderMapper.queryOrderList(userId);
    }

    @Override
    public void deleteOrder(long rid) {
        orderMapper.deleteOrder(rid);
    }
}
