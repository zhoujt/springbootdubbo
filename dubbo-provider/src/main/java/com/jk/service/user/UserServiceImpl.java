package com.jk.service.user;

import com.alibaba.dubbo.config.annotation.Service;

import com.jk.mapper.user.UserMapper;
import com.jk.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Service(interfaceClass = UserService.class )
@Component
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> selectAllUser() {
        return userMapper.selectAllUser();
    }

    @Override
    public int saveUser(User user) {
      return  userMapper.saveUser(user);
    }

    @Override
    public User findUserById(Integer id) {
        return userMapper.findUserById(id);
    }

    @Override
    public void deleteUserById(Integer id) {
         userMapper.deleteUserById(id);
    }

    @Override
    public int updateUser(User user) {
       return userMapper.updateUser(user);
    }

    @Override
    public User findUserByUsername(String principal) {
        return userMapper.findUserByUsername(principal);
    }

    @Override
    public List<String> queryPowerKeyList(Long id) {
        List<String> lists = new ArrayList<String>();

        lists   =  userMapper.queryPowerKeyList(id);
        return lists;

    }

    @Override
    public User selectUserByPhone(String phone) {
        return userMapper.selectUserByPhone(phone);
    }
}
