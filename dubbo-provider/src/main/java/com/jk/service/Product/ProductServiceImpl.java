package com.jk.service.Product;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.Product.ProductMapper;
import com.jk.model.brand.Brand;
import com.jk.model.color.Color;
import com.jk.model.goods.Goods;
import com.jk.model.product.Middle;
import com.jk.model.product.Product;
import com.jk.model.product.Treet;
import com.jk.model.type.Type;
import com.jk.model.unit.Unit;
import com.jk.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = ProductService.class)
@Component
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductMapper productMapper;


    @Override
    public List<Product> findAllProduct(Product product) {
        return productMapper.findAllProduct(product);
    }

    @Override
    public void addProduct(Product product) {
        productMapper.addProduct(product);
    }

    @Override
    public void deleteProduct(long pid) {
        productMapper.deleteProduct(pid);
    }



    @Override
    public void edit(Product product) {
        productMapper.edit(product);
    }

    @Override
    public Product queryproductOrderById(Long pid) {
        return productMapper.queryproductOrderById(pid);
    }

    @Override
    public void addinsertProduct(Goods goods) {
        productMapper.addinsertProduct(goods);
    }

    @Override
    public List<Unit> unittypelist() {
        return productMapper.unittypelist();
    }

    @Override
    public List<Color> colortypelist() {
        return productMapper.colortypelist();
    }

    @Override
    public void tomiddle(Goods goods) {
        productMapper.tomiddle(goods);
    }

    @Override
    public List<Goods> centerlist() {
        return productMapper.centerlist();
    }

    @Override
    public void tomiddlecenter(Middle middle) {
        productMapper.tomiddlecenter(middle);
    }

    @Override
    public User getUserListt2(String loginName) {
        return productMapper.getUserListt2(loginName);
    }

    @Override
    public void toUser(User user) {
        productMapper.toUser(user);
    }


    @Override
    public List<Product> goosderById(Long pid) {
        return productMapper.goosderById(pid);
    }

    @Override
    public List<Treet> totreelist(Integer id) {

        List<Treet> treelist = childTree(id);
        return treelist;
    }

    private List<Treet> childTree(Integer id) {
        List<Treet> ltree = productMapper.getTreeFs(id);
        for (Treet tree : ltree) {
            Integer id2 = tree.getId();
            List<Treet> treelist = childTree(id2);
            if(treelist == null || treelist.size() <=0){
                tree.setSelectable(true);
            }
            else {
                tree.setSelectable(false);
                tree.setNodes(treelist);
            }
        }
        return ltree;
    }



    @Override
    public List<Brand> brandlist() {
        return productMapper.brandlist();
    }

    @Override
    public List<Type> typelist() {
        return productMapper.typelist();
    }

   /* @Override
    public Product queryproductOrderById(Product product) {
        return productMapper.queryproductOrderById(product);
    }*/


}
