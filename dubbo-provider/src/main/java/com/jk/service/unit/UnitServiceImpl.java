package com.jk.service.unit;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.unit.UnitMapper;
import com.jk.model.unit.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Service(interfaceClass = UnitService.class)
@Component
public class UnitServiceImpl implements UnitService {
    @Autowired
    private UnitMapper unitMapper;
    @Override
    public List<Unit> selectUnitList(Unit unit) {
        List<Unit> list = unitMapper.selectUnitList(unit);
        return list;
    }

    @Override
    public String queryUnitPid() {
        return unitMapper.queryUnitPid();
    }

    @Override
    public void saveUnit(Unit unit) {
        unit.setUnitType("商品管理-销售单位");
        unitMapper.saveUnit(unit);
    }

    @Override
    public void deleteUnit(Long unitId) {
        unitMapper.deleteUnit(unitId);
    }
}
