package com.jk.service.type;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.type.TypeMapper;
import com.jk.model.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = TypeService.class)
@Component
public class TypeServiceImpl implements TypeService{
    @Autowired
    private TypeMapper typeMapper;
    @Override
    public List<Type> selectTypeTree() {
        List<Type> types = typeMapper.queryTypeById();
        return types;
    }

    @Override
    public void addTypeByPid(Type type) {
        int str = -1;
        type.setPid(Long.valueOf(str));
        typeMapper.addTypeByPid(type);
    }

    @Override
    public void addById(Type type) {
        int str1 = -1;
        if (type.getPid() == null){
            type.setPid(Long.valueOf(str1));
        }
        typeMapper.addById(type);
    }

}
