package com.jk.service.goods;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.goods.GoodsMapper;
import com.jk.model.center.GoodsCenter;
import com.jk.model.goods.Goods;
import com.jk.model.order.Order;
import com.jk.model.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = GoodsService.class)
@Component
public class GoodsServiceImpl implements GoodsService{

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public List<Goods> selectGoods() {
        return goodsMapper.selectGoods();
    }

    @Override
    public Goods selectGoodsById(Long gid) {
        return goodsMapper.selectGoodsById(gid);
    }

    @Override
    public List<Goods> selectColor() {
        return goodsMapper.selectColor();
    }

    @Override
    public List<Goods> selectBrand() {
        return goodsMapper.selectBrand();
    }

    @Override
    public List<Goods> selectproduct() {
        return goodsMapper.selectproduct();
    }

    @Override
    public List<Type> selectTypeTree() {
        return goodsMapper.selectTypeTree();
    }

    @Override
    public void insertGoods(Goods goods) {
        goodsMapper.insertGoods(goods);
    }

    @Override
    public void deleteCompary(Long gid) {
         goodsMapper.deleteCompary(gid);
    }

    @Override
    public void updateBrandAll(Long bid, Long[] ids) {
        goodsMapper.updateBrandAll(bid,ids);
    }

    @Override
    public void updateTypeAll(Long tid, Long[] ids) {
        goodsMapper.updateTypeAll(tid,ids);
    }

    @Override
    public void uploadBlog(String uploadUrl,Long gid) {
        goodsMapper.uploadBlog(uploadUrl,gid);
    }

    @Override
    public List<GoodsCenter> querygsize() {
        return goodsMapper.querygsize();
    }

    @Override
    public List<Map<String, Object>> queryReportBin() {
        return goodsMapper.queryReportBin();
    }

    @Override
    public Map<String, Object> queryBrokenLine() {
        List<Order> list = goodsMapper.queryBrokenLine();
        String[] userNameArr = new String[list.size()];
        Long[] clickArr = new Long[list.size()];
        for (int i = 0; i < list.size(); i++) {
            userNameArr[i]=list.get(i).getUserPlayName();
            clickArr[i]=list.get(i).getLoveClickCount();
        }
        Map<String,Object> clickMap = new HashMap<String,Object>();
        clickMap.put("categories", userNameArr);
        clickMap.put("data", clickArr);
        return clickMap;
    }
}
