package com.jk.service.homepage;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.homepage.HomeMapper;
import com.jk.model.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = HomeService.class)
@Component
public class HomeServiceImpl implements HomeService{
    @Autowired
    private HomeMapper homeMapper;

    @Override
    public List<Type> selectTypeByPid() {
        return homeMapper.selectTypeByPid();
    }

    @Override
    public List<Type> selectTypeById() {
        return homeMapper.selectTypeById();
    }
}
