package com.jk.service.color;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.color.GoodsColorMapper;
import com.jk.model.color.Color;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Service(interfaceClass =GoodsColorService.class)
@Component
public class GoodsColorServiceImpl implements GoodsColorService {
    @Autowired
    private GoodsColorMapper goodsColorMapper;

    @Override
    public List<Color> seleGoodsColorList() {

        return goodsColorMapper.seleGoodsColorList();
    }

    @Override
    public void deleteById(Integer id) {
        goodsColorMapper.deleteById(id);
    }

    @Override
    public List<Color> seleListPid() {
        return goodsColorMapper.seleListPid();
    }

    @Override
    public void addList(Color color) {
        goodsColorMapper.addList(color);
    }


}
