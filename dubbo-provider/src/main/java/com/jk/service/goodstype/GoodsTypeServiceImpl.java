package com.jk.service.goodstype;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.goodstype.GoodsTypeMapper;
import com.jk.model.brand.Brand;
import com.jk.model.color.Color;
import com.jk.model.goods.Goods;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
@Service(interfaceClass = GoodsTypeService.class)
public class GoodsTypeServiceImpl implements GoodsTypeService {
    @Resource
    private GoodsTypeMapper goodsTypeMapper;
    @Override
    public List<Goods> selecttypelist(Long id,String bvalue,String cvalue) {
        List<Goods> goodtypelist =goodsTypeMapper.selecttypelist(id,bvalue,cvalue);
        return goodtypelist;
    }

    @Override
    public List<Brand> selectbrandlist() {
        return goodsTypeMapper.selectbrandlist();
    }



    @Override
    public List<Color> selectcolorlist() {
        return goodsTypeMapper.selectcolorlist();
    }

    @Override
    public List<Goods> selecByPrice() {
        return goodsTypeMapper.selecByPrice();
    }
}
