package com.jk.service.tree;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.tree.TreeMapper;
import com.jk.model.tree.Tree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = TreeService.class )
@Component
public class TreeServiceImpl implements TreeService {
    @Autowired
    private TreeMapper treeMapper;
    @Override
    public List<Tree> selectAllTree() {
        String pid ="0";
        List<Tree> treelist = childTree(pid);
        return treelist;
    }

    private List<Tree> childTree(String pid) {
        List<Tree> ltree = treeMapper.queryTreeById();
        for (Tree tree : ltree) {
            List<Tree> treelist = childTree(String.valueOf(tree.getId()));
            tree.setNodes(treelist);
        }
        return ltree;
    }
}
