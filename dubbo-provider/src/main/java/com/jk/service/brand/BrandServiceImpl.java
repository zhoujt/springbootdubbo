package com.jk.service.brand;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.brand.BrandMapper;
import com.jk.model.brand.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = BrandService.class)
@Component
public class BrandServiceImpl implements BrandService {
    @Autowired
    private BrandMapper brandMapper;
    @Override
    public List<Brand> selectBrandList(Brand brand) {
        return brandMapper.selectBrandList(brand);
    }

    @Override
    public void saveBrand(Brand brand) {
        brandMapper.saveBrand(brand);
    }

    @Override
    public Brand selectBrandById(Long bid) {
        return brandMapper.selectBrandById(bid);
    }

    @Override
    public void removeBrand(Long bid) {
        brandMapper.removeBrand(bid);
    }

    @Override
    public void updateBrand(Brand brand) {
        brandMapper.updateBrand(brand);
    }
}
